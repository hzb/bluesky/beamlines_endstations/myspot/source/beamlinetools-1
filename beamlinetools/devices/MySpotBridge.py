from ophyd import EpicsSignal, Device
from ophyd import Component as Cpt

class MySpotBridge(Device):
    is_ramping = Cpt(EpicsSignal,'IsRamping')