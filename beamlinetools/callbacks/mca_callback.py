from beamlinetools.BEAMLINE_CONFIG.base import *
from beamlinetools.callbacks.csv_exporter import *

import os
import h5py


from bluesky.callbacks import CallbackBase

class McaCallback(CallbackBase):
    """Callback for the MCA detector.

    This callback save the mca data in the 
    correct Experiment/set folder.
    It runs only if in the detectors list there is 'mca'
    When the stop document is created the data from mca1 is saved
    We should decide how to include the other mca detectors,
    and how to deal with scans that move more than one motor 
    (at the moment the callback would simply fail)

    """    
    def stop(self, doc):
        run = db[-1]
        meta = run.baseline.metadata 
        try:
            motor_name = meta['start']['motors'][0]
            detectors = meta['start']['detectors']
        except KeyError:
            return
        if 'mca' in detectors:
            scan = run.primary.read()
            mca = scan['mca_mca1_spectrum']

            motor_pos = scan[motor_name]
            mca_folder = os.path.join(mds._subject_dir, 'mca')

            if not os.path.exists(mca_folder):
                print(f"Save folder {mca_folder}")
                os.makedirs(mca_folder)
            scan_id = "{:06d}".format(RE.md['scan_id'])
            mca_filename = os.path.join(mca_folder, mds._subject+'_'+scan_id+'.h5')
            with h5py.File(mca_filename, 'w') as hf:
                scan = hf.create_group('1D Scan')
                dset = hf.create_dataset("1D Scan/XRF data",  data=mca)
                dset.attrs.create('DATASET_TYPE', "Raw XRF counts")
                
                dset = hf.create_dataset("1D Scan/X Positions",  data=motor_pos)
                dset.attrs.create('DATASET_TYPE', "X")
                dset.attrs.create('Motor info', motor_name)
        else:
            pass