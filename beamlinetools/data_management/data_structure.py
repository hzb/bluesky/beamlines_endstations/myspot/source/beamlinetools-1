import time
import os
from beamlinetools.BEAMLINE_CONFIG.base import RE
from beamlinetools.BEAMLINE_CONFIG.file_export import *
import socket

class MySpotDataStructure:
    """
    This class is used to create the datastructure needed at MySpot

    The class should be initialized in a startup file like this:
    from beamlinetools.data_management.data_structure import MySpotDataStructure    
    mds = MySpotDataStructure()
    
    Then from the ipython session one can create a new experiment
    mds.new_experiment(<'experimental_group'>)
    
    For differen parts of the experiment a new folder with a new specfile
    can be created with:
    mds.newdata(<'experiment_description'>)

    The class takes care also of the persistence of different spec-scan numbers.

    """    

    def __init__(self):
        self._group_name = None
        self._set_number = 1
        self._bluesky_data_path = None

    def new_experiment(self, group_name=None):
        if group_name != None:
            self._group_name = group_name
        self._bluesky_data_path = '/opt/bluesky/data'
        self._experiment_dir = os.path.join(self._bluesky_data_path, time.strftime('%Y-%m-%d')+'_'+self._group_name)
        # Check if the directory exists
        if not os.path.exists(self._experiment_dir):
            # If it doesn't exist, create it
            os.makedirs(self._experiment_dir)
        self._set_number = 1
        
    def newdata(self,subject, set_n=None):
        RE.md['hostname'] = socket.gethostname()
        print(f"RE.md['hostname'] {RE.md['hostname']}")
        if set_n == None:
            set_n = self._set_number

        self._subject     = time.strftime('%Y-%m-%d')+'_set'+"{:03d}".format(set_n)+'_'+subject
        self._subject_dir = os.path.join(self._experiment_dir,self._subject)
        if not os.path.exists(self._subject_dir):
            # If it doesn't exist, create it
            os.makedirs(self._subject_dir)
            os.makedirs(os.path.join(self._subject_dir,'persistence'))
            print(f'Created new folder {self._subject_dir}')
            RE.md['scan_id']=0
            self._set_number += 1
            
            
        else:
            print(f'Existing folder detected {self._subject_dir}')
            
        new_spec_file(self._subject_dir, self._subject)
        spec_factory.file_prefix = self._subject
        spec_factory.directory = self._subject_dir
        
        RE.md['specfile']=self._subject_dir
        self.pd = PersistentDict(os.path.join(self._subject_dir,'persistence'))
        RE.md = self.pd
        if 'scan_id' in self.pd.keys():
            print(f'Next scan is number: {RE.md["scan_id"]+1}')
        else:
            print(f'Next scan is number: {0}')


        